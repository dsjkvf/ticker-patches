#!/bin/sh

curl -sL https://api.github.com/repos/097115/ticker-forked/releases/latest \
   | grep -i $(echo $OSTYPE | sed 's/\([a-z]*\).*/\1/') \
   | grep url \
   | cut -d ':' -f2,3 \
   | tr -d '"' \
   | xargs curl -sLO
