#!/bin/bash

rm -f ticker*

if ! echo $(git rev-parse --show-toplevel) | grep -iq forked
then
    # reset
    git reset --hard

    # set the patches location
    dir=$(dirname $0)

    # apply patches
    echo "Applying 0001-feat-total-value-is-now-also-displayed-in-the-footer.patch..."
    git apply $dir/0001-feat-total-value-is-now-also-displayed-in-the-footer.patch
    [ $? -eq 0 ] && echo "Done" || exit 1
    echo "Applying 0002-perf-watchlist.go-WIDTH_CHANGE_STATIC-set-to-18.patch..."
    git apply $dir/0002-perf-watchlist.go-WIDTH_CHANGE_STATIC-set-to-18.patch
    [ $? -eq 0 ] && echo "Done" || exit 1
fi

# check version
printf "\033[1;36mVersion? \033[0m" && read VER; sed -i.bak "s/0.0.0/$VER/" cmd/root.go && printf "\n"

# build
echo "Building linux-32..."
go build -o ticker-linux-32
echo "Done"
echo "Building linux-64..."
GOOS=linux GOARCH=amd64 go build -o ticker-linux-64
echo "Done"
echo "Building darwin-64..."
GOOS=darwin GOARCH=amd64 go build -o ticker-darwin-64
echo "Done"
