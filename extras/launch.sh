#!/bin/bash

if hostname | grep -iq penelope
then
    /usr/bin/scp -r -q "$HOME/.ticker.yaml" "s@pluto:/home/s/.ticker.yaml"
else
    /usr/bin/scp -r -q "s@pluto:/home/s/.ticker.yaml" "$HOME/.ticker.yaml"
fi
eval TZ=America/New_York $(find $HOME/usr/local/bin -iname 'ticker*' | head -n 1) --show-holdings
